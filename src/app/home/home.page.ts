import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  cards!: { img: string; title: string; description: string; }[];
  originalCards: { img: string; title: string; description: string; }[] = [];

  constructor() {
    this.initializeCards();

    // this.cards = [];
  }
  initializeCards() {


    const cardsData = [
      {
        img: "assets/images/image-1.jpeg",
        title: "Card 1",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
      },
      {
        img: "assets/images/image-2.jpeg",
        title: "Card 2",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
      },
      {
        img: "assets/images/image-3.jpeg",
        title: "Card 3",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
      },
      {
        img: "assets/images/image-4.jpeg",
        title: "Card 4",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
      },
      // {
      //   img: "assets/images/test1.png",
      //   title: "Card 5",
      //   description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
      // }
    ]
    this.cards = JSON.parse(JSON.stringify(cardsData));
    this.originalCards = JSON.parse(JSON.stringify(cardsData));
  }
  resetCards() {
    this.cards = JSON.parse(JSON.stringify(this.originalCards));
  }

  logChoice(choice: any) {
    console.log(choice);
    if (!this.cards.length) {
      this.resetCards();
    }
  }

}

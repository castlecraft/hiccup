import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CardSwiperPagePageRoutingModule } from './card-swiper-page-routing.module';

import { CardSwiperPagePage } from './card-swiper-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardSwiperPagePageRoutingModule
  ],
  declarations: [CardSwiperPagePage]
})
export class CardSwiperPagePageModule {}

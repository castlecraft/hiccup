import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CardSwiperPagePage } from './card-swiper-page.page';

describe('CardSwiperPagePage', () => {
  let component: CardSwiperPagePage;
  let fixture: ComponentFixture<CardSwiperPagePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(CardSwiperPagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccountCreationPageRoutingModule } from './account-creation-routing.module';

import { AccountCreationPage } from './account-creation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccountCreationPageRoutingModule
  ],
  declarations: [AccountCreationPage]
})
export class AccountCreationPageModule {}

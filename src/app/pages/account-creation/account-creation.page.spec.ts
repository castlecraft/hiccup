import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AccountCreationPage } from './account-creation.page';

describe('AccountCreationPage', () => {
  let component: AccountCreationPage;
  let fixture: ComponentFixture<AccountCreationPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(AccountCreationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

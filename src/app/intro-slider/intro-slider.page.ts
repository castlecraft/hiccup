import { Component, OnInit, AfterViewInit } from '@angular/core';
import Swiper from 'swiper';
import { SwiperOptions } from 'swiper/types/swiper-options';

@Component({
  selector: 'app-intro-slider',
  templateUrl: './intro-slider.page.html',
  styleUrls: ['./intro-slider.page.scss'],
})
export class IntroSliderPage implements OnInit, AfterViewInit {
  private swiper: Swiper | undefined;

  constructor() {
    this.swiper = undefined;
  }

  ngOnInit() {
    this.initializeSwiper();
  }

  ngAfterViewInit() {
    console.log('ngAfterViewInit is called');

    this.initializeSwiper();
  }

  private initializeSwiper() {
    const swiperConfig: SwiperOptions = {
      slidesPerView: 'auto', // Change to a valid numeric value if needed
      spaceBetween: 10,
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',  // Specify pagination type as bullets
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    };
    
    this.swiper = new Swiper('.swiper-container', swiperConfig);

  }
}

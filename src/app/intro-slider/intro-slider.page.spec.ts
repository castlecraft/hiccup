import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IntroSliderPage } from './intro-slider.page';

describe('IntroSliderPage', () => {
  let component: IntroSliderPage;
  let fixture: ComponentFixture<IntroSliderPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(IntroSliderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import Swiper from 'swiper';

@Component({
  selector: 'app-slides',
  templateUrl: './slides.component.html',
  styleUrls: ['./slides.component.scss']
})
export class SlidesComponent implements AfterViewInit {
  @ViewChild('swiperContainer')
  swiperContainer!: ElementRef;
  @ViewChild('swiperPagination')
  swiperPagination!: ElementRef;
  swiper!: Swiper;

  constructor(private router: Router) {}

  ngAfterViewInit() {
    this.swiper = new Swiper(this.swiperContainer.nativeElement, {
      slidesPerView: 1,
      pagination: {
        el: this.swiperPagination.nativeElement,
        clickable: true
      },
      speed: 400
    });
  }

  goToNextSlide() {
    if (this.swiper) {
      console.log(this.swiper)
      if (this.swiper.activeIndex ===2) {
        this.router.navigate(['/home']);
      } else {
        this.swiper.slideNext();  // Advance to the next slide
      }
    }
  }

  navigateToLogin() {
    this.router.navigate(['/account-creation']);
  }

  hideNextButton() {
    console.log(this.swiper?.activeIndex)
    return true
  }
}

import { Component, ElementRef, EventEmitter, Input, OnInit, Output, QueryList, Renderer2, ViewChildren } from '@angular/core';

@Component({
  selector: 'app-tinder-ui',
  templateUrl: './tinder-ui.component.html',
  styleUrls: ['./tinder-ui.component.scss'],
})
export class TinderUiComponent  implements OnInit {
  @Input('cards')
  cards: Array<{
    img: string;
    title: string;
    description: string;
  }> = [];
  swiping: boolean = false;

  @ViewChildren('tinderCard') tinderCards!: QueryList<ElementRef>;
  tinderCardsArray: Array<ElementRef> = [];

  @Output() choiceMade = new EventEmitter();

  @Output() cardsEmptied = new EventEmitter<void>();

  moveOutWidth!: number;
  shiftRequired: boolean = false;
  transitionInProgress: boolean = false;
  heartVisible: boolean = false;
  crossVisible: boolean = false;

  constructor(private renderer: Renderer2) {}
  ngOnInit(): void {
  }

  userClickedButton(event: { preventDefault: () => void; },heart: boolean) {
    event.preventDefault();
    if (!this.cards.length) return;
    if (heart) {
      this.renderer.setStyle(
        this.tinderCardsArray[0].nativeElement,
        'transform',
        'translate(' + this.moveOutWidth + 'px, -100px) rotate(-30deg)'
      );
      this.toggleChoiceIndicator(false, true);
      this.emitChoice(heart, this.cards[0]);
    } else {
      this.renderer.setStyle(
        this.tinderCardsArray[0].nativeElement,
        'transform',
        'translate(-' + this.moveOutWidth + 'px, -100px) rotate(30deg)'
      );
      this.toggleChoiceIndicator(true, false);
      this.emitChoice(heart, this.cards[0]);
    }
    this.shiftRequired = true;
    this.transitionInProgress = true;
  }

  swipeLeft() {
    // logic for swiping left (probably a 'no' choice)
    this.userClickedButton({ preventDefault: () => {} }, false);
  }
  
  swipeRight() {
    // logic for swiping right (probably a 'yes' choice)
    this.userClickedButton({ preventDefault: () => {} }, true);
  }
  
  handlePan(event: any) {
    this.swiping = true; // Set swiping to true at the start of the pan.
    if (
      event.deltaX === 0 ||
      (event.center.x === 0 && event.center.y === 0) ||
      !this.cards.length
    )
      return;

    if (this.transitionInProgress) {
      this.handleShift();
    }

    this.renderer.addClass(this.tinderCardsArray[0].nativeElement, 'moving');

    if (event.deltaX > 0) {
      this.toggleChoiceIndicator(false, true);
    }
    if (event.deltaX < 0) {
      this.toggleChoiceIndicator(true, false);
    }

    let xMulti = event.deltaX * 0.03;
    let yMulti = event.deltaY / 80;
    let rotate = xMulti * yMulti;

    this.renderer.setStyle(
      this.tinderCardsArray[0].nativeElement,
      'transform',
      'translate(' +
        event.deltaX +
        'px, ' +
        event.deltaY +
        'px) rotate(' +
        rotate +
        'deg)'
    );

    this.shiftRequired = true;
  }

  handlePanEnd(event: any) {
    this.swiping = false; // Set swiping to false at the end of the pan.
    this.toggleChoiceIndicator(false, false);

    if (!this.cards.length) return;

    this.renderer.removeClass(this.tinderCardsArray[0].nativeElement, 'moving');

    let keep = Math.abs(event.deltaX) < 80 || Math.abs(event.velocityX) < 0.5;
    if (keep) {
      this.renderer.setStyle(
        this.tinderCardsArray[0].nativeElement,
        'transform',
        ''
      );
      this.shiftRequired = false;
    } else {
      let endX = Math.max(
        Math.abs(event.velocityX) * this.moveOutWidth,
        this.moveOutWidth
      );
      let toX = event.deltaX > 0 ? endX : -endX;
      let endY = Math.abs(event.velocityY) * this.moveOutWidth;
      let toY = event.deltaY > 0 ? endY : -endY;
      let xMulti = event.deltaX * 0.03;
      let yMulti = event.deltaY / 80;
      let rotate = xMulti * yMulti;

      this.renderer.setStyle(
        this.tinderCardsArray[0].nativeElement,
        'transform',
        'translate(' +
          toX +
          'px, ' +
          (toY + event.deltaY) +
          'px) rotate(' +
          rotate +
          'deg)'
      );

      this.shiftRequired = true;

      this.emitChoice(!!(event.deltaX > 0), this.cards[0]);
    }
    this.transitionInProgress = true;
  }

  toggleChoiceIndicator(cross: boolean, heart: boolean) {
    this.crossVisible = cross;
    this.heartVisible = heart;
  }

  handleShift() {
    if (this.swiping) {
      // Return early if we're currently swiping.
      return;
  }
    this.transitionInProgress = false;
    this.toggleChoiceIndicator(false, false);
    if (this.shiftRequired) {
      this.shiftRequired = false;
      this.cards.shift();
    }
    if (this.cards.length === 0) { // Because we're about to remove the last card
      this.cardsEmptied.emit();
    }
  }

  emitChoice(heart: boolean, card: { img: string; title: string; description: string; }) {
    this.choiceMade.emit({
      choice: heart,
      payload: card,
    });

    if (this.cards.length === 0) { // Because we're about to remove the last card
        this.cardsEmptied.emit();
    }

    if (this.cards.length === 1) { // Because we're about to remove the last card
      this.toggleChoiceIndicator(false, false);
    }
  }

  ngAfterViewInit() {
    this.moveOutWidth = document.documentElement.clientWidth * 1.5;
    this.tinderCardsArray = this.tinderCards.toArray();
    console.log(this.tinderCardsArray)
    console.log('=======>', JSON.parse(JSON.stringify(this.tinderCards)));
    this.tinderCards.changes.subscribe(() => {
      this.tinderCardsArray = this.tinderCards.toArray();
      console.log(
        '=======>',
        JSON.parse(JSON.stringify(this.tinderCardsArray))
      );
    });
  }

}

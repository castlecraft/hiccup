import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.page.html',
  styleUrls: ['./welcome.page.scss'],
})
export class WelcomePage implements OnInit {
  isLoading: boolean=true;

  constructor() { }
  ngOnInit() {
    setTimeout(() => {
      this.isLoading = false;
    }, 2000); // 10000 milliseconds = 10 seconds
  }
}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'welcome',
    pathMatch: 'full'
  },
  {
    path: 'welcome',
    loadChildren: () => import('./welcome/welcome.module').then( m => m.WelcomePageModule)
  },
  {
    path: 'intro-slider',
    loadChildren: () => import('./intro-slider/intro-slider.module').then( m => m.IntroSliderPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./pages/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'card-swiper-page',
    loadChildren: () => import('./pages/card-swiper-page/card-swiper-page.module').then( m => m.CardSwiperPagePageModule)
  },
  {
    path: 'account-creation',
    loadChildren: () => import('./pages/account-creation/account-creation.module').then( m => m.AccountCreationPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

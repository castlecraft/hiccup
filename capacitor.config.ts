import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.hiccup.hiccup',
  appName: 'hiccup',
  webDir: 'www',
  server: {
    androidScheme: 'https'
  }
};

export default config;
